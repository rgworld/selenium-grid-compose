# selenium-grid-compose

---
##To run test on local machine
These tests run on selenium-standalone server on local 
 which needs to be installed before we run it on 
 local machine.
 
I have added installation script within `package.json` file.

To install `selenium-standalone` server, follow these steps

1. Run `npm run selenium:install`
2. Run `npm run selenium:start`
on a separate terminal
3. Run `npm test` to start a single `chrome` session and test
4. Run `npm run test:multi` to start multi browser session

at this moment, chrome and firefox are configured to run.

Note: Both binaries should be installed on your system.

---
## To run test on docker
You can also run these tests using `docker-compose`, which will make it run on selenium grid

1. Run `docker-compose run codeceptjs` to run test on docker containers.

It will create selenium grid with 1 instance of `chrome` and `firefox`.

*Limitation*:
At this moment, as official `codeceptjs` docker image is not configured to run `run-multiple` command, so it will start test one single instance of chrome on grid. 

---
## Gitlab CI Integration
This repo is configured to run tests on every commit made on this repo.

---
Nice to have:

1. Update `codeceptjs` image with custom runner to have `run-multiple` 
2. Use docker image `cache` to decrease build time.