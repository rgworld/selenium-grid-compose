
const I = actor();

module.exports = {

    root:'.app-atc-layer-redesign-content-wrapper',

    // setting locators
    fields: {
        closeBtn: '.viicon-close',
        goToCartBtn: "a:contains('Go to cart')"
        //.atc-layer-container .vi-VR-btnWdth-XL.btn-scnd
    }
};
