let webDriverConfig = require('./webdriver.conf.js'); // reading selenium config from separate file

webDriverConfig.host = process.env.SELENIUM_HOST || 'localhost'; // choosing local vs. docker selenium
webDriverConfig.port = process.env.SELENIUM_PORT || 4444; // running local and docker on different ports to avoid conflics
console.log('Selenium config', 'Host:', webDriverConfig.host, 'Port:', webDriverConfig.port);

exports.config = {
    helpers: {
        WebDriverIO: webDriverConfig
    },
    include: {
        "I": "./steps_file.js",
        "landingPage": "./pages/landing.js",
        "searchResultPage": "./pages/searchResult.js",
        "searchItemPage": "./pages/searchItem.js",
        "addToCartFragment": "./fragments/addToCart.js"
    },
    multiple: {
        "basic": {
            // run all tests in chrome and firefox
            "browsers": ["chrome", "firefox"]
        },
    },
    mocha: {},
    bootstrap: false,
    teardown: null,
    hooks: [],
    gherkin: {},
    tests: "./test/*_test.js",
    timeout: 10000,
    name: "dr-auto-test1",
    output: "./output",
};