module.exports = {
    restart: false,
    windowSize: "maximize",
    smartWait: 10000,
    keepCookies: true,
    keepBrowserState: true,
    host: 'localhost',
    port: 4444,
    timeouts: {
        "script": 60000,
        "page load": 60000
    },
    url: "https://www.ebay.com.au/",
    browser: "chrome",
    coloredLogs: true,
    waitForTimeout: 10000
};