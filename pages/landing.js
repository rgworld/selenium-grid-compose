
const I = actor();

module.exports = {

    // setting locators
    fields: {
        searchObj: '#gh-ac',
        cartObj: '#gh-cart-i',
        searchBtn:'#gh-btn',
        cartBtn: '#gh-cart'
    },

    doSearch(strSearchTxt) {
        I.say('Searching for product');
        I.fillField(this.fields.searchObj,strSearchTxt);
        I.click(this.fields.searchBtn);
    },

};
