
const I = actor();

module.exports = {

    // setting locators
    fields: {
        searchResultLayer: '.srp-results',
        searchResultLnk: '.s-item__title:first-child'
    },

    selectFirstItem() {
        within(this.fields.searchResultLayer,()=>{
            I.waitForVisible(this.fields.searchResultLnk);
            I.click(this.fields.searchResultLnk);
        });
    }
};
