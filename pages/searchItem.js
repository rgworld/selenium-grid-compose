
const I = actor();
const addToCartFragment = require('../fragments/addToCart.js');

module.exports = {

    // setting locators
    fields: {
        addToCartBtn: '#atcRedesignId_btn'
    },

    addItemToCart() {
        I.say('Adding first product to cart');
        I.click(this.fields.addToCartBtn);

        I.say('I see product is added to cart');
        I.waitForVisible(addToCartFragment.fields.closeBtn);

        I.say('I close cart layer');
        I.click(addToCartFragment.fields.closeBtn);
    }
};
