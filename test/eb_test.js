// <reference path="./steps.d.ts" />
Feature('Add product to cart');

Scenario('Add 2 products to cart and verify',
    (I, landingPage, searchResultPage, searchItemPage) => {

    I.amOnPage('/');
    landingPage.doSearch('apple watch');
    searchResultPage.selectFirstItem();
    searchItemPage.addItemToCart();

    I.amOnPage('/');
    landingPage.doSearch('thermos');
    searchResultPage.selectFirstItem();
    searchItemPage.addItemToCart();

    I.say('Checking Cart items');
    I.click(landingPage.fields.cartBtn);
    I.see('Shopping cart (2 items)');

});


